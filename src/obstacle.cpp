#include <cost_map_manager/obstacle.hpp>
using namespace cost_map_manager;

Obstacle::Obstacle()
{}
Obstacle::Obstacle(double radius, double duration, geometry_msgs::PointStamped position, ros::Time time_init)
{

  setRadius(radius);
  setDuration(duration);
  setPosition(position);
  setTime_init(time_init);
}

Obstacle::~Obstacle()
{

}


geometry_msgs::PointStamped Obstacle::getPosition() const
{
  return position;
}

void Obstacle::setPosition(const geometry_msgs::PointStamped &value)
{
  position = value;
}


double Obstacle::getRadius() const
{
  return radius;
}

void Obstacle::setRadius(double value)
{
  radius = value;
}

double Obstacle::getDuration() const
{
  return duration;
}

void Obstacle::setDuration(double value)
{
  duration = value;
}

ros::Time Obstacle::getTime_init() const
{
  return time_init;
}

void Obstacle::setTime_init(const ros::Time &value)
{
  time_init = value;
}

