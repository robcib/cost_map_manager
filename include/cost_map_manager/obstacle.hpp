#ifndef OBSTACLE_HPP
#define OBSTACLE_HPP

#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
namespace cost_map_manager {

class Obstacle
{
  public:

    Obstacle();
    Obstacle(double radius, double duration, geometry_msgs::PointStamped position, ros::Time time_init = ros::Time::now());
    ~Obstacle();

    ros::Time getTime_init() const;
    void setTime_init(const ros::Time &value);
    double getDuration() const;
    void setDuration(double value);
    double getRadius() const;
    void setRadius(double value);
    geometry_msgs::PointStamped getPosition() const;
    void setPosition(const geometry_msgs::PointStamped &value);
  private:
    geometry_msgs::PointStamped position;
    ros::Time time_init;
    double duration;
    double radius;

};

}

#endif // OBSTACLE_HPP
